<?php


namespace App\Orders;


use App\Models\Order;

class Calculator
{
    protected $order;
    
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function value()
    {
        return $this->order->orderDetails->sum( function ($detail) {

            $product = $detail->product;

            $quantity = $detail->quantity;

            return $product->amount * $quantity;
        });
    }
}
