<?php


namespace App\Http\Controllers;


use App\Repositories\DeliveryRepository;

class DeliveryController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new DeliveryRepository();
    }

    public function fetch()
    {
        return $this->response($this->repo->deliveries());
    }

    public function show($id)
    {
        return $this->response($this->repo->show($id));
    }
}
