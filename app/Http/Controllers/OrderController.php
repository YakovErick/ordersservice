<?php


namespace App\Http\Controllers;


use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new OrderRepository();
    }

    public function fetch()
    {
        return $this->response($this->repo->orders());
    }

    public function store(Request $request)
    {
        $result = $this->repo->create($request->all());

        return $this->response($result, Response::HTTP_CREATED);
    }

    public function show($id)
    {
        return $this->response($this->repo->show($id));
    }

    public function update(Request $request, $id)
    {
        $result = $this->repo->update($request->all(), $id);

        return $this->response($result);
    }

    public function destroy($id)
    {
        return $this->response($this->repo->destroy($id));
    }
}