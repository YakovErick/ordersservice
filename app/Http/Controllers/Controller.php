<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ApiResponse;

    public function response($data, $code = Response::HTTP_OK)
    {
        return $this->successResponse($data, $code);
    }
}
