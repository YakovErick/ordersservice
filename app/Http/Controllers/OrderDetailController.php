<?php


namespace App\Http\Controllers;


use App\Repositories\OrderDetailRepository;
use Illuminate\Http\Request;

class OrderDetailController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new OrderDetailRepository();
    }

    public function fetch()
    {
        return $this->response($this->repo->orderDetails());
    }

    public function show($id)
    {
        return $this->response($this->repo->show($id));
    }

    public function update(Request $request, $id)
    {
        $result = $this->repo->update($request->all(), $id);

        return $this->response($result);
    }

    public function destroy($id)
    {
        return $this->response($this->repo->destroy($id));
    }

    public function markAsDelivered(Request $request)
    {
        return $this->response($this->repo->markDelivered($request->all()));
    }
}
