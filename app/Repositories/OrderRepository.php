<?php


namespace App\Repositories;


use App\Models\Order;
use App\Traits\Paginate;
use App\Transformers\OrderTransformer;
use Carbon\Carbon;
use Illuminate\Http\Response;

class OrderRepository
{
    use Paginate;

    public function orders()
    {
        return $this->sortFilterPaginate(

            new Order(),

            [],

            function ($order) {
                return app(OrderTransformer::class)->transform($order);
            },

            function ($model) {
                return $model->whereHas('orderDetails');
            }
        );
    }

    public function create($data)
    {
        if(isset($data['order_id'])) {

            $order = Order::findOrFail($data['order_id']);

            $order->orderDetails()->createMany($data['items']);

            return [ 'message' => 'Order items updated successfully!' ];
        }

        $order = Order::create([
            'headquarter_id' => random_int(1, 5), //TODO remove hardcoded headquarter id
            'date' => Carbon::now()
        ]);

        $order->orderDetails()->createMany($data['items']);

        return [ 'message' => 'Order created successfully!' ];
    }

    public function show($id)
    {
        $order = Order::findOrFail($id);

        return (new OrderTransformer())->transform($order);
    }

    public function update($data, $id)
    {
        $order = Order::findOrFail($id);

        if ($order->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $order->update($data);

        return [ 'message' => 'Order updated successfully!' ];
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $order->orderDetails->each( function ($detail) {
            $detail->delete();
        });

        $order->delete();

        return [ 'message' => 'Order deleted successfully!' ];
    }
}
