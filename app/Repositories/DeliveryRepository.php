<?php


namespace App\Repositories;


use App\Models\Delivery;
use App\Traits\Paginate;
use App\Transformers\DeliveryTransformer;

class DeliveryRepository
{
    use Paginate;

    public function deliveries()
    {
        return $this->sortFilterPaginate(

            new Delivery(),

            [],

            function ($delivery) {
                return app(DeliveryTransformer::class)->transform($delivery);
            },

            function ($model) {
                return $model->whereHas('orderDeliveries');
            }
        );
    }

    public function show($id)
    {
        $delivery = Delivery::findOrFail($id);

        return (new DeliveryTransformer())->transform($delivery);
    }
}
