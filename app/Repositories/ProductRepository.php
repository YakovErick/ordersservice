<?php


namespace App\Repositories;


use App\Models\Product;
use App\Traits\FormValidation;
use App\Traits\Paginate;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Response;

class ProductRepository
{
    use Paginate, FormValidation;

    public function products()
    {
        return $this->sortFilterPaginate(

            new Product(),

            [],

            function ($product) {
                return app(ProductTransformer::class)->transform($product);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function create($data)
    {
        $this->validateProduct($data);

        if(isset($data['id'])) {
            $product = Product::findOrFail($data['id']);

            $product->update($data);

            return [ 'message' => 'Product updated successfully!' ];
        }

        $data['slug'] = str_slug($data['name']);

        Product::create($data);

        return [ 'message' => 'Product created successfully!' ];
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);

        return (new ProductTransformer())->transform($product);
    }

    public function update($data, $id)
    {
        $product = Product::findOrFail($id);

        if ($product->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $product->update($data);

        return [ 'message' => 'Product updated successfully!' ];
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return [ 'message' => 'Product deleted successfully!' ];
    }

    public function all()
    {
        return Product::all();
    }
}
