<?php


namespace App\Repositories;


use App\Models\Delivery;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderDetailDelivery;
use App\Traits\Paginate;
use App\Transformers\OrderDetailTransformer;
use Carbon\Carbon;
use Illuminate\Http\Response;

class OrderDetailRepository
{
    use Paginate;

    public function orderDetails()
    {
        return $this->sortFilterPaginate(

            new OrderDetail(),

            [],

            function ($detail) {
                return app(OrderDetailTransformer::class)->transform($detail);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function show($id)
    {
        $details = OrderDetail::findOrFail($id);

        return (new OrderDetailTransformer())->transform($details);
    }

    public function update($data, $id)
    {
        $details = OrderDetail::findOrFail($id);

        $details->update($data);

        return [ 'message' => 'Order Detail updated successfully!' ];
    }

    public function destroy($id)
    {
        $details = OrderDetail::findOrFail($id);

        $details->delete();

        return [ 'message' => 'Order Detail deleted successfully!' ];
    }

    public function markDelivered($data)
    {
        if(!isset($data['items'])) {
            return [
                'message' => 'Select order items and proceed'
            ];
        }

        $items = isset($data['items']) ? $data['items'] : [];

        foreach($items as $item) {

            $balance = isset($item['balance']) ? $item['balance'] : 0;

            $order_item = OrderDetail::findOrFail($item['id']);

            $this->createOrderDetailDelivery($order_item, $balance);
        }

        return [
            'message' => 'Created successfully!'
        ];
    }

    public function createOrderDetailDelivery(OrderDetail $order_item, $balance)
    {
        $delivery = $this->createDelivery($order_item);

        $delivered = $order_item->orderDeliveries->sum('quantity');

        $bal = $delivered > $order_item->quantity ? 0 : $order_item->quantity - $delivered;

        $balance = $balance > 0 ? $balance : $bal;

        if($balance == 0) {
            return $order_item;
        }

        return OrderDetailDelivery::create([
            'order_detail_id' => $order_item->id,
            'delivery_id' =>  $delivery->id,
            'quantity' => $balance
        ]);
    }

    public function createDelivery(OrderDetail $item)
    {
        $product = $item->product;

        return Delivery::create([
            'supplier_id' => $product->supplier->id,
            'date' => Carbon::now(),
            'status' => true
        ]);
    }
}
