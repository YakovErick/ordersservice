<?php


namespace App\Repositories;


use App\Models\Supplier;
use App\Traits\FormValidation;
use App\Traits\Paginate;
use App\Transformers\SupplierTransformer;
use Illuminate\Http\Response;

class SupplierRepository
{
    use Paginate, FormValidation;

    public function suppliers()
    {
        return $this->sortFilterPaginate(

            new Supplier(),

            [],

            function ($supplier) {
                return app(SupplierTransformer::class)->transform($supplier);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function create($data)
    {
        $this->validateSupplier($data);

        if(isset($data['id'])) {

            $supplier = Supplier::findOrFail($data['id']);

            $supplier->update($data);

            return [ 'message' => 'Supplier created successfully!' ];
        }

        Supplier::create($data);

        return [ 'message' => 'Supplier created successfully!' ];
    }

    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);

        return (new SupplierTransformer())->transform($supplier);
    }

    public function update($data, $id)
    {
        $supplier = Supplier::findOrFail($id);

        if ($supplier->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $supplier->update($data);

        return [ 'message' => 'Supplier updated successfully!' ];
    }

    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);

        $supplier->delete();

        return [ 'message' => 'Supplier deleted successfully!' ];
    }
}
