<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'deliveries';

    protected $guarded = ['id'];

    protected $fillable = [
        'supplier_id',
        'date',
        'status'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function orderDeliveries()
    {
        return $this->hasMany(OrderDetailDelivery::class, 'delivery_id');
    }
}
