<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $table = 'suppliers';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'phone',
        'email',
        'address'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'supplier_id');
    }
}
