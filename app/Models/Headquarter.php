<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Headquarter extends Model
{
    protected $table = 'headquarters';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'address'
    ];

    public function branches()
    {
        return $this->hasMany(Branch::class, 'headquarter_id');
    }
}
