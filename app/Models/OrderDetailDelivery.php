<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderDetailDelivery extends Model
{
    protected $table = 'order_detail_deliveries';

    protected $guarded = ['id'];

    protected $fillable = [
        'order_detail_id',
        'delivery_id',
        'quantity'
    ];

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class, 'order_detail_id');
    }
}
