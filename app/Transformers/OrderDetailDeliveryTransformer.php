<?php


namespace App\Transformers;


use App\Models\OrderDetailDelivery;
use Carbon\Carbon;

class OrderDetailDeliveryTransformer
{
    public function transform(OrderDetailDelivery $detail)
    {
        $product = $detail->orderDetail->product;

        return [
            'id' => $detail->id,
            'product' => $detail->orderDetail->product->name,
            'quantity' => $detail->quantity,
            'date' => Carbon::parse($detail->updated_at)->toDateTimeString(),
            'value' => $product->amount * $detail->quantity
        ];
    }
}
