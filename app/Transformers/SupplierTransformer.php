<?php


namespace App\Transformers;


use App\Models\Supplier;

class SupplierTransformer
{
    public function transform(Supplier $supplier)
    {
        return [
            'id' => $supplier->id,
            'name' => $supplier->name,
            'phone' => $supplier->phone,
            'email' => $supplier->email,
            'address' => $supplier->address,
            'products' => $this->products($supplier)
        ];
    }

    public function products(Supplier $supplier)
    {
        return $supplier->products->map( function ($product) {
            return (new ProductTransformer())->transform($product);
        });
    }
}
