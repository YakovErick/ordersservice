<?php


namespace App\Transformers;


use App\Models\Delivery;
use Carbon\Carbon;

class DeliveryTransformer
{
    public function transform(Delivery $delivery)
    {
        $orderDelivery = $delivery->orderDeliveries()->first();

        $value = $this->calculate($delivery);

        return [
            'id' => $delivery->id,
            'date' => Carbon::parse($delivery->date)->toDateTimeString(),
            'order_date' => $orderDelivery ? $orderDelivery->orderDetail->order->date : null,
            'supplier' => $delivery->supplier->name,
            'items' => $delivery->orderDeliveries->count(),
            'deliveries' => $this->deliveries($delivery),
            'value' => $value
        ];
    }

    public function deliveries(Delivery $delivery)
    {
        return $delivery->orderDeliveries->map( function ($detail) {
            return (new OrderDetailDeliveryTransformer())->transform($detail);
        });
    }

    public function calculate(Delivery $delivery)
    {
        return $delivery->orderDeliveries->sum( function ($delivery_detail) {

            $product =  $delivery_detail->orderDetail->product;

            return $product->amount * $delivery_detail->quantity;
        });
    }
}
