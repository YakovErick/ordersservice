<?php


namespace App\Transformers;


use App\Models\OrderDetail;
use Carbon\Carbon;

class OrderDetailTransformer
{
    public function transform(OrderDetail $detail)
    {
        $delivered = $detail->orderDeliveries->sum('quantity');

        $balance = $delivered > $detail->quantity ? 0 : $detail->quantity - $delivered;

        return [
            'id' => $detail->id,
            'order_id' => $detail->order->id,
            'product' => $detail->product->name,
            'product_id' => $detail->product->id,
            'quantity' => $detail->quantity,
            'number' => (int) $detail->quantity,
            'delivered' => $delivered > $detail->quantity ? (int) $detail->quantity : (int) $delivered,
            'balance' => $balance,
            'date' => Carbon::parse($detail->updated_at)->toFormattedDateString(),
            'value' => $detail->quantity * $detail->product->amount,
            'value_per_item' => $detail->product->amount,
        ];
    }
}
