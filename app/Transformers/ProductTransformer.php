<?php


namespace App\Transformers;


use App\Models\Product;

class ProductTransformer
{
    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'amount' => $product->amount,
            'description' => $product->description,
            'slug' => $product->slug,
            'supplier' => $product->supplier ? $product->supplier->name : null,
            'supplier_id' => $product->supplier ? $product->supplier->id : null
        ];
    }
}
