<?php


namespace App\Transformers;


use App\Models\Order;
use App\Orders\Calculator;
use Carbon\Carbon;

class OrderTransformer
{
    public function transform(Order $order)
    {
        $calculator = new Calculator($order);

        return [
            'id' => $order->id,
            'order_details' => $this->details($order),
            'date' => Carbon::parse($order->updated_at)->toFormattedDateString(),
            'items' => $order->orderDetails->sum('quantity'),
            'value' => $calculator->value(),
            'headquarter' => $order->headquarter->name,
            'branch' => $order->headquarter->branches()->first()->name
        ];
    }

    public function details(Order $order)
    {
        $details = $order->orderDetails;

        return $details->map( function ($detail) {
            return (new OrderDetailTransformer())->transform($detail);
        });
    }
}
