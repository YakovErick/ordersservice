<?php


namespace App\Traits;


use Illuminate\Support\Facades\Validator;

trait FormValidation
{
    public function validateSupplier($data)
    {
        return Validator::make( $data, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required'
        ]);
    }

    public function validateProduct($data)
    {
        return Validator::make( $data, [
            'name' => 'required',
            'description' => 'required',
            'supplier_id' => 'required'
        ]);
    }
}