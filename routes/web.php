<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('/suppliers', 'SupplierController@fetch');
    $router->get('/suppliers/{id}', 'SupplierController@show');
    $router->post('/suppliers', 'SupplierController@store');
    $router->patch('/suppliers/{id}', 'SupplierController@update');
    $router->put('/suppliers/{id}', 'SupplierController@update');
    $router->delete('/suppliers/{id}', 'SupplierController@destroy');
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('/products', 'ProductController@fetch');
    $router->get('/products/{id}', 'ProductController@show');
    $router->post('/products', 'ProductController@store');
    $router->patch('/products/{id}', 'ProductController@update');
    $router->put('/products/{id}', 'ProductController@update');
    $router->delete('/products/{id}', 'ProductController@destroy');
    $router->get('/all-products', 'ProductController@all');
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('/orders', 'OrderController@fetch');
    $router->get('/orders/{id}', 'OrderController@show');
    $router->post('/orders', 'OrderController@store');
    $router->patch('/orders/{id}', 'OrderController@update');
    $router->put('/orders/{id}', 'OrderController@update');
    $router->delete('/orders/{id}', 'OrderController@destroy');
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('/order-details', 'OrderDetailController@fetch');
    $router->get('/order-details/{id}', 'OrderDetailController@show');
    $router->post('/order-details', 'OrderDetailController@store');
    $router->patch('/order-details/{id}', 'OrderDetailController@update');
    $router->put('/order-details/{id}', 'OrderDetailController@update');
    $router->delete('/order-details/{id}', 'OrderDetailController@destroy');

    $router->post('/order-details/delivery/save', 'OrderDetailController@markAsDelivered');
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('/deliveries', 'DeliveryController@fetch');
    $router->get('/deliveries/{id}', 'DeliveryController@show');
});
