<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

        factory(\App\Models\Supplier::class, 20)->create();
        factory(\App\Models\Product::class, 40)->create();
        factory(\App\Models\Delivery::class, 40)->create();
        factory(\App\Models\OrderDetailDelivery::class, 40)->create();
        factory(\App\Models\OrderDetail::class, 40)->create();
        factory(\App\Models\Order::class, 20)->create();
        factory(\App\Models\Headquarter::class, 5)->create();
        factory(\App\Models\Branch::class, 20)->create();
    }
}
