<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Faker\Generator;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});


$factory->define( \App\Models\Supplier::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'address' => $faker->address
    ];
});

$factory->define( \App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'slug' => $faker->slug,
        'supplier_id' => $faker->numberBetween(1, 20),
        'amount' => $faker->numberBetween(50, 20000)
    ];
});

$factory->define(\App\Models\Delivery::class, function (Faker\Generator $faker) {
    return [
        'supplier_id' => $faker->numberBetween(1, 20),
        'status' => $faker->numberBetween(0, 1),
        'date' => $faker->dateTime
    ];
});

$factory->define(\App\Models\OrderDetailDelivery::class, function (Faker\Generator $faker) {
    return [
        'order_detail_id' => $faker->numberBetween(1, 40),
        'delivery_id' => $faker->numberBetween(1, 20),
        'quantity' => $faker->numberBetween(1, 20),
    ];
});

$factory->define(\App\Models\OrderDetail::class, function (Faker\Generator $faker) {
    return [
        'order_id' => $faker->numberBetween(1, 20),
        'product_id' => $faker->numberBetween(1, 40),
        'quantity' => $faker->numberBetween(20, 100)
    ];
});

$factory->define(\App\Models\Order::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->dateTime,
        'headquarter_id' => $faker->numberBetween(1, 5)
    ];
});

$factory->define(\App\Models\Headquarter::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address
    ];
});

$factory->define(\App\Models\Branch::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'headquarter_id' => $faker->numberBetween(1, 5)
    ];
});
